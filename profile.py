"""Allocate one or both NVIDIA Grace Hopper nodes. This profile installs the
Cloudlab base Ubuntu22 image for these nodes (which includes the NVIDIA 64K
Linux 6.5 kernel) and optionally, the NVIDIA GPU driver and CUDA toolkit as
described in: https://docs.nvidia.com/grace-ubuntu-install-guide.pdf.
"""

# Import the Portal object.
import geni.portal as portal
# Import the ProtoGENI library.
import geni.rspec.pg as pg
# Emulab specific extensions.
import geni.rspec.emulab as emulab

# Create a portal context, needed to defined parameters
pc = portal.Context()

# Create a Request object to start building the RSpec.
request = pc.makeRequestRSpec()

# Variable number of nodes.
pc.defineParameter("nodeCount", "Number of Nodes (1 or 2)",
                   portal.ParameterType.INTEGER, 1,
                   longDescription="If you specify more then one node, " +
                   "they will be put in a LAN for you.")

# Install recommened NVIDIA software packages for GH
pc.defineParameter("nvidiasw", "Install NVIDIA CUDA software",
                   portal.ParameterType.BOOLEAN, True,
                   longDescription="Installs the NVIDIA recommended GPU " +
                   "driver and CUDA toolkit for the Grace Hopper. " +
                   "(Reboots the machine after install.)")

# Retrieve the values the user specifies during instantiation.
params = pc.bindParameters()

# Check parameter validity.
if params.nodeCount < 1 or params.nodeCount > 2:
    pc.reportError(portal.ParameterError("You must choose 1 or 2 nodes.", ["nodeCount"]))

image = "urn:publicid:IDN+clemson.cloudlab.us+image+emulab-ops//UBUNTU22-64-ARM9"
ntype = "nvidiagh"

# Create link/lan.
if params.nodeCount > 1:
    link = request.Link()
    lan.best_effort = True
    pass

# Process nodes, adding to link.
for i in range(params.nodeCount):
    name = "node" + str(i)
    node = request.RawPC(name)
    node.disk_image = image
    node.hardware_type = ntype

    if params.nvidiasw:
        command = "sudo sh /local/repository/scripts/startup.sh"
        node.addService(pg.Execute(shell="sh", command=command))
        pass

    # Add to lan
    if params.nodeCount > 1:
        iface = node.addInterface("eth1")
        link.addInterface(iface)
        pass

    pass

# Print the RSpec to the enclosing page.
pc.printRequestRSpec(request)
