#!/bin/sh
#
# Install CUDA software ala:
# https://docs.nvidia.com/grace-ubuntu-install-guide.pdf
# Appendix A.

# currently running kernel
kcurrent=$(uname -r)

# kernel + pkg that indicates there is no need to run this script
targetkernel=6.5.0-1015-nvidia-64k
targetpkg=cuda-toolkit-12.2

# kernel + pkg that indicates we are running the necessary 6.5 HWE kernel
basekernel=6.5.0-1015-nvidia-64k
basepkg=linux-nvidia-64k-hwe-22.04

# See if our script has already run or everything is already installed
if [ $kcurrent = $targetkernel ] && dpkg -V $targetpkg >/dev/null 2>&1; then
    echo "Already running the right kernel and Cuda packages installed."
    exit 0
fi

# Otherwise the node should be running the 6.5 HWE kernel or the nvidia.ko
# module loaded below will cause great grief! (CPU lockups)
if [ ! $kcurrent = $basekernel ] || ! dpkg -V $basepkg >/dev/null 2>&1; then
    echo "Must be running $basekernel kernel with $basepkg installed!"
    exit 1
fi

if ! dpkg -V cuda-keyring >/dev/null 2>&1; then
    kring='https://developer.download.nvidia.com/compute/cuda/repos/ubuntu2204/sbsa/cuda-keyring_1.1-1_all.deb'
    wget $kring || {
	echo "Could not download cuda-keyring: $kring!"
	exit 1
    }
    dpkg -i cuda-keyring*.deb || {
	echo "Could not install cuda-keyring!"
	exit 1
    }
fi

# XXX Before the CUDA kernel modules get built, we need to make gcc-12 the
# default compiler due to gcc-11 not recognizing "-fsanitize=shadow-call-stack"
# which the modules get built with.
gcc=""
if [ -e /usr/bin/gcc ]; then
    gcc=$(/usr/bin/gcc -v 2>&1 | grep '^gcc version 12')
fi
if [ -z "$gcc" ]; then
    apt-get update
    apt-get install -y gcc-12 || {
	echo "Could not install gcc-12"
	exit 1
    }
    update-alternatives --install /usr/bin/gcc gcc /usr/bin/gcc-12 12
fi

if ! dpkg -V cuda-toolkit-12-2 >/dev/null 2>&1; then
    apt-get update
    apt-get install -y cuda-toolkit-12-2 || {
	echo "Could not install cuda-toolkit!"
	exit 1
    }
    apt-get install -y nvidia-kernel-open-535 cuda-drivers-535 || {
	echo "Could not install nvidia kernel or Cuda drivers!"
	exit 1
    }

    #
    # Need to reboot after this is done
    #
    sync
    reboot now
fi

exit 0
